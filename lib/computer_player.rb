class ComputerPlayer
  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    @board.print_board
  end

  def get_move
    available_pos = []
    @board.grid.length.times do |row|
      @board.grid[row].length.times do |col|
        pos = [row, col]
        available_pos << pos if @board.empty?(pos)
      end
    end

    available_pos.each do |pos|
      if winning_move?(pos)
        return pos
      end
    end

    available_pos[rand(available_pos.length)]
  end

  private

  def winning_move?(pos)
    @board.place_mark(pos, @mark)
    if @board.winner == @mark
      @board.place_mark(pos, nil)
      return true
    end
    @board.place_mark(pos, nil)
    false
  end

end
