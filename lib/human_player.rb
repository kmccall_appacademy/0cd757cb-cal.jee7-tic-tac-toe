class HumanPlayer
  attr_reader :name
  attr_accessor :mark, :board

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
    @board.print_board
  end

  def get_move
    puts "Where would you like to place your mark?"
    input = gets.chomp
    move_string = input.strip.split(",")
    move_string.map(&:to_i)
  end

end
