class Board
  attr_reader :grid

  def initialize(grid=[[nil] * 3, [nil] * 3, [nil] * 3])
    @grid = grid
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def print_board
    @grid.each { |row| puts row.to_s }
    puts "------------------"
  end

  def winner
    # check rows
    @grid.each do |row|
      winning_mark = row[0]
      next if winning_mark.nil?
      return winning_mark if row.all? { |mark| mark == winning_mark }
    end

    # check columns
    @grid[0].length.times do |col|
      winning_mark = @grid[0][col]
      next if winning_mark.nil?
      return winning_mark if @grid.all? { |row| row[col] == winning_mark }
    end

    # check diagonals
    diag_mark_1 = @grid[0][0]
    return diag_mark_1 if diag_mark_1 == @grid[1][1] && diag_mark_1 == @grid[2][2]
    diag_mark_2 = @grid[0][2]
    return diag_mark_2 if diag_mark_2 == @grid[1][1] && diag_mark_2 == @grid[2][0]

    # no winner
    nil
  end

  def over?
    # check if winner
    return true if !winner.nil?

    # check if grid is filled
    @grid.each do |row|
      return false if row.any?(&:nil?)
    end

    true
  end
end
