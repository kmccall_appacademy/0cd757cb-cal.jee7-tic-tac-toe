require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
  end

  def play_turn
    @player_one.display(@board)
    @player_two.display(@board)
    current_player_move = @current_player.get_move
    @board.place_mark(current_player_move, @current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play_game
    until @board.winner || @board.over?
      play_turn
    end
    @board.print_board
  end

end
#
# c1 = HumanPlayer.new("c1")
# c2 = ComputerPlayer.new("c2")
# c1.mark = :x
# c2.mark = :o
# game = Game.new(c1, c2)
# game.play_game
